/**
 * PalindromTest
 */
public class PalindromTest {
   private char[] s = null;
   private String testWord;

   public PalindromTest(String[] args) {
       testWord = args[0];
       s = new char[testWord.length()];

       for (int i = 0; i < testWord.length(); i++) {
           s[i] = testWord.charAt(i);
       }
   }

   public boolean ispali(char[] s) {
       for (int i = 0; i < (s.length / 2);) {
           // rentner
           if (s[i] != s[s.length - 1]) {
               System.out.println(testWord + " ist kein Palindrom");
               return false;
           }
           i++;
       }
       return true;
   }

   public void start() {
       if (ispali(s) == true) {
           System.out.println(testWord + " ist ein Palindrom");
       }
   }

   public static void main(String[] args) {
       if (args.length == 0) {
           System.out.println("Bitte ein Wort eingeben");
       }
       new PalindromTest(args).start();
   }
}