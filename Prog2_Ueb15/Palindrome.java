interface iPalindrom {
   boolean istPalindrom(String wort);
}

public class Palindrome {
   public class PalindromRekursiv implements iPalindrom {
      public boolean istPalindrom(char[] chs, int is, int ie) {
         if (ie < is)
            return true;
         if (chs[is] == chs[ie])
            return istPalindrom(chs, ++is, --ie);
         return false;
      }

      public boolean istPalindrom(String wort) {
         char[] ch = wort.toLowerCase().toCharArray();
         int i = 0;
         int j = wort.length() - 1;

         return istPalindrom(ch, i, j);
      }
   }

   public class PalindromIterativ implements iPalindrom {
      public boolean istPalindrom(String wort) {
         char[] chs = wort.toLowerCase().toCharArray();
         int i = 0;
         int j = wort.length() - 1;
         while (j > i) {
            if (chs[i] != chs[j])
               return false;
            i++;
            j--;
         }
         return true;
      }
   }

   public void main2(String[] args) {
      long nt1, nt2;
      int rep = 1000;
      String ts = "ottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottoottootto";
      boolean b = false;
      iPalindrom palindrom = new PalindromIterativ();
      //iPalindrom palindrom = new PalindromRekursiv();
      nt1 = System.nanoTime();
      for (int i = 0; i < rep; i++) 
      {
         b = palindrom.istPalindrom(ts);
      }
      nt2 = System.nanoTime();
      System.out.printf("'%s' ist palindrom: %b, nt:%d\n", ts, b, (nt2 - nt1) / rep);
   }

   public static void main(String[] args) {
      Palindrome palindrom = new Palindrome();
      palindrom.main2(args);
   }
}