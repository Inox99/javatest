/**
 * PatientenWarteschlange
 */
public class Patient {
   private int pnr;
   private String name;

   public Patient(int pnr, String name) {
      // es wird noch nicht geprüft ob patient mit name [name] oder nummer [pnr] schon existiert
      if (pnr < 1000 || pnr >= 10000) {
         throw new IllegalStateException("Patientennummer ist nicht 4 stellig");
      }
      this.pnr = pnr;

      if (name.length() == 0) {
         throw new IllegalStateException("Name ist null");
      }
      this.name = name.trim();
   }

   public String getName() {
      return name;
   }

   public int getPnr() {
      return pnr;
   }
}