/*
   bitbucket
   https://bitbucket.org/Inox99/javatest/src/master/t_static.java

*/

class Haus {
   final public static double StromProKWh = 0.33;
   final public static double ZaehlerAbleseGebuehr = 35.50;
   public static double StromGrundgebuehrProJahr = 15.0 * 12;
   public int StromVerbrauchInKWh;

   // wenn von dieser Klasse objekte angelegt werden dann gibt es einen
   // (unterschiedlichen) Wert für StromVerbrauchInKW pro Objekt (Instanz)
   //
   // die Werte für die static - variablen gibt es jedoch für alle Objekte
   // (Instanzen) aber nur einmalig dadurch natürlich auch für alle gleich
   //
   // das schlüsselwort final bedeutet: dieser Wert kann nicht im programm geändert
   // werden
   //
   public double StromVariablekosten() {
      return StromVerbrauchInKWh * StromProKWh + StromGrundgebuehrProJahr;
   }

   public static double StromFixkosten() {
      // in statischen Methoden kann man nur auf statische Member zugreifen
      // instanzen member gehen da nicht! weil es a ggf gar keine Instanzen gibt
      // (geben muss)
      return StromGrundgebuehrProJahr + ZaehlerAbleseGebuehr;
   }

   public static double StromGesamtVariablekosten(Haus[] haeuser) {
      // wnn man aber Instanzen-members braucht, kann man alle Instanzen als Parameter
      // übergeben
      double Kosten = 0.0;
      for (Haus haus : haeuser) {
         Kosten += haus.StromVariablekosten();
      }
      return Kosten;
   }

   public static double StromGesamtFixkosten(Haus[] haeuser) {
      return haeuser.length * Haus.StromFixkosten();
   }

   public static double StromGesamtVerbrauchKWh(Haus[] haeuser) {
      double d = 0.0;
      for (Haus haus : haeuser) {
         d += haus.StromVerbrauchInKWh;
      }
      return d;
   }
}

public class t_static {

   public static void main(String[] args) {
      Haus haus1 = new Haus();
      Haus haus2 = haus1;
      haus1.StromVerbrauchInKWh = 1111;
      haus2.StromVerbrauchInKWh = 2222;
      // zeigt den jeweiligen Stromverbrauch an, denkt man
      // haus1.StromVerbrauchInKWh wäre 1111 und
      // haus2.StromVerbrauchInKWh wäre 2222
      // dem ist aber nicht so, sondern
      System.out.printf("haus1 StromVerbrauchInKWh: %d\n", haus1.StromVerbrauchInKWh);
      System.out.printf("haus2 StromVerbrauchInKWh: %d\n", haus2.StromVerbrauchInKWh);

      // das ist bei Objekten also anders als bei einfachen Type wie Zahlen
      double d1 = 0;
      double d2 = d1;
      d1 = 1111.1;
      d2 = 2222.2;
      System.out.printf("d1: %f\n", d1);
      System.out.printf("d2: %f\n", d2);
      // hier ist es also wie man denkt.

   }

   public static void main2(String[] args) {
      Haus[] haeuser;
      double stromKosten = 0.0;
      if (args.length == 0) {
         System.out.printf("t_static d [d [d] ..]\n");
         System.out.printf(
               "\td ist jeweils ein double wert der den Jahres-Stromverbauch pin KW pro Haus angibt, davon können beliebig viele parameter angegeben sein\n");
         System.out.printf("\tBeispiel: t_static 2500 3300 4700 \n");
         return;
      }
      haeuser = new Haus[args.length];
      for (int i = 0; i < args.length; i++) {
         // hier müssen wir strings in int-zahl umwandeln
         // und zwar für jedes haus einzeln
         haeuser[i] = new Haus();
         haeuser[i].StromVerbrauchInKWh = Integer.parseInt(args[i]);
      }
      stromKosten = 0.0;
      for (Haus haus : haeuser) {
         stromKosten = stromKosten + haus.StromVariablekosten() + Haus.StromFixkosten();
      }
      System.out.printf("Stromkosten für %d Häuser\n", haeuser.length);
      System.out.printf("\t bestehend aus Fixkosten: %d * %f€ = %f€\n", haeuser.length, Haus.StromFixkosten(),
            haeuser.length * Haus.StromFixkosten());
      System.out.printf("\t variable Kosten: %fKWh * %f€ = %f€\n", Haus.StromGesamtVerbrauchKWh(haeuser),
            Haus.StromProKWh, Haus.StromGesamtVariablekosten(haeuser));
      System.out.printf("\t Summe: %.2f€\n",
            Haus.StromGesamtFixkosten(haeuser) + Haus.StromGesamtVariablekosten(haeuser));
   }
}
