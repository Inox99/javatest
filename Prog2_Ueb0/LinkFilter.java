import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javax.swing.plaf.synth.SynthToggleButtonUI;

/**
 * LinkFilter
 */
public class LinkFilter {
   Pattern pattern = Pattern.compile("(https|http)\\:\\/\\/(\\w+|[w]{3})\\.[a-z0-9]*\\.[a-z]{2,3}");
   //private File file;
   private Scanner scan = null;
   //private Matcher matcher;
   private File[] fileTab;
   private int totalNumberOfMatches = 0;

   public String inputFileName() {
      Scanner input = new Scanner(System.in);
      System.out.print("Dateiname: ");
      String fileName = input.nextLine();
      input.close();
      return fileName.trim();
   }

   public File pathFile(String fileName) {
      return null;
   }

   public File[] inputFile() {
      // fileTab[inputFileName().length()] = new File(inputFileName());
      fileTab[0] = new File(inputFileName());
      return fileTab;
   }

   public void readFile(File file) {
      String line;
      int numberOfLines = 0;
      int numberOfMatches = 0;

      try {
         scan = new Scanner(file);
         while (scan.hasNextLine()) {
            line = scan.nextLine();
            numberOfLines++;
            Matcher m = pattern.matcher(line);
            if (m.find()) {
               numberOfMatches++;
               System.out.printf("/3d: /s/n", numberOfLines, line);
            }
         }
      } catch (FileNotFoundException e) {
         System.out.println(e);
      } catch (Exception e) {
         System.out.println(e);
      } finally {
         if (numberOfMatches > 0) {
            totalNumberOfMatches += numberOfMatches;
            System.out.printf("/s: Number of matches: /d/n/n", file.getName(), numberOfMatches);
         }
      }
   }

   public void start() {
      for (File file : fileTab) {
         readFile(file);
      }
      System.out.printf("Total number of matches: /d/n", totalNumberOfMatches);
   }

   public static void main(String[] args) {
      new LinkFilter().start();
   }
}