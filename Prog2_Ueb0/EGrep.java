
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EGrep {
   private File[] fileTab;
   private Scanner input = null;
   private Pattern pattern;
   private int totalNumberOfMatches = 0;

   public EGrep(String[] args) {
      System.out.printf("Searching %s ...%n", args[0]);
      pattern = Pattern.compile(args[0]);
      fileTab = new File[args.length - 1];
      for (int i = 0; i < fileTab.length; ++i) {
         fileTab[i] = new File(args[i + 1]);
      }
   }

   public void start() {
      for (File file : fileTab) {
         scanFile(file);
      }
      System.out.printf("Total number of matches: %d%n", totalNumberOfMatches);
   }

   public void scanFile(File file) {
      String line;
      int numberOfLines = 0;
      int numberOfMatches = 0;

      try {
         input = new Scanner(file);
         while (input.hasNextLine()) {
            line = input.nextLine();
            numberOfLines++;
            Matcher m = pattern.matcher(line);
            while (m.find()) {
               numberOfMatches++;
               System.out.printf("%3d: %s%n", numberOfLines, line);
            }
         }
      } catch (FileNotFoundException e) {
         System.out.println(e);
      } catch (Exception e) {
         System.out.println(e);
      } finally {
         if (input != null) {
            input.close();
         }
         if (numberOfMatches > 0) {
            totalNumberOfMatches += numberOfMatches;
            System.out.printf("%s: Number of matches: %d%n%n", file.getName(), numberOfMatches);
         }
      }
   }

   public static void main1(String[] args) {
      // java egrep [0-9]{4,} datei1.txt
      if (args.length > 1) {
         new EGrep(args).start();
      } else {
         // System.out.println("Gebrauch: java EGrep regex datei1 ...");
         System.out.println("Gebrauch: java EGrep regexAusdruck dateiname1 [... dateiname2 ]");
      }
   }

   public static void main2(String[] args) {
      System.out.println("main2");
      System.out.println(args.toString());
   }

   public static void main(String[] args) {
      main2(args);
   }
}