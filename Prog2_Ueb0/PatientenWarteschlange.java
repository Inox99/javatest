public class PatientenWarteschlange {
   private Patient[] pw;

   public PatientenWarteschlange(int groesse) {
      if (groesse > 10) {
         throw new IllegalStateException("Warteschlange ist größer als 10");
      }
      pw = new Patient[groesse];
   }

   public void neuerPatient(int pnr, String name) {
      for (int i = 0; i < pw.length; i++) {
         if (pw[i] == null) {
            pw[i] = new Patient(pnr, name);
            return;
         }
      }
   }

   public void entfernePatient(int pnr) {
      for (int i = 0; i < pw.length; i++) {
         // if (pw[i].getPnr() == pnr) {
         if (pw[i] != null && pw[i].getPnr() == pnr) {
            // pw[i] = null;
            pw[i] = pw[getLaengeSchlange()];
            pw[getLaengeSchlange()] = null;
         }
      }
   }

   public Patient derNaechsteBitte() {
      Patient derNaechste = pw[0];
      return derNaechste;

   }

   public int getLaengeSchlange() {
      for (int i = 0; i < pw.length; i++) {
         if (pw[i] == null) {
            return i - 1;
         }
      }
      return -1;
   }

   public String ToString(){
      return "";
   }
   public void Ausgabe() {
      for (int i = 0; pw[i] != null; i++) {
         System.out.printf(" i: %d , pnr: %d, name:%s\n", i, pw[i].getPnr(), pw[i].getName());
      }
      System.out.printf("----\n");
   }

   public static class PWTest {
      public static void main(String[] args) {
         PatientenWarteschlange patientenWarteschlange = new PatientenWarteschlange(10);
         patientenWarteschlange.neuerPatient(1000, "hans");
         patientenWarteschlange.neuerPatient(1001, "franz");
         patientenWarteschlange.neuerPatient(1002, "otto");
         patientenWarteschlange.Ausgabe();

         // patientenWarteschlange.entfernePatient(0);
         System.out.printf("entfernePatient(1001)\n");
         patientenWarteschlange.entfernePatient(1001);
         patientenWarteschlange.Ausgabe();
      }
   }
}
/*
 * ausgangssitiuation
 * 
 * alle = null pw[0] pw[1] pw[2] pw[3] pw[4] pw[5] pw[6] pw[7] pw[8] pw[9]
 * 
 * 
 * 
 */